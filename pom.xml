<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ D:/Development/XNAT/1.6/nrg_anonymize/pom.xml
  ~ XNAT http://www.xnat.org
  ~ Copyright (c) 2014, Washington University School of Medicine
  ~ All Rights Reserved
  ~
  ~ Released under the Simplified BSD.
  ~
  ~ Last modified 8/27/13 11:57 AM
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.nrg</groupId>
        <artifactId>parent</artifactId>
        <version>1.7.0-SNAPSHOT</version>
    </parent>

    <artifactId>anonymize</artifactId>

    <name>NRG Anonymize Framework</name>
    <description>Common API For Anonymizing Dicom Images</description>

    <scm>
        <connection>scm:hg:https://bitbucket.org/nrg/nrg_anonymize</connection>
        <url>scm:hg:https://bitbucket.org/nrg/nrg_anonymize</url>
    </scm>

    <issueManagement>
        <system>JIRA</system>
        <url>https://issues.xnat.org</url>
    </issueManagement>

    <licenses>
        <license>
            <name>Simplified BSD License</name>
            <url>http://www.opensource.org/licenses/BSD-2-Clause</url>
        </license>
    </licenses>

    <organization>
        <name>Neuroinformatics Research Group</name>
        <url>http://nrg.wustl.edu</url>
    </organization>

    <properties>
        <maven.server.root>https://nrgxnat.artifactoryonline.com/nrgxnat</maven.server.root>
    </properties>

    <dependencies>
        <!--BEGIN  NRG dependencies -->
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>transaction</artifactId>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>framework</artifactId>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>DicomEdit</artifactId>
            <scope>compile</scope>
        </dependency>
        <!--END  NRG dependencies -->

        <dependency>
            <groupId>org.antlr</groupId>
            <artifactId>antlr-runtime</artifactId>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>dcm4che</groupId>
            <artifactId>dcm4che-core</artifactId>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>dcm4che</groupId>
            <artifactId>dcm4che-iod</artifactId>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate.javax.persistence</groupId>
            <artifactId>hibernate-jpa-2.1-api</artifactId>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>cglib</groupId>
            <artifactId>cglib</artifactId>
        </dependency>
        <dependency>
            <groupId>org.javassist</groupId>
            <artifactId>javassist</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
        </dependency>

        <!-- BEGIN Test dependencies -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.sf.ehcache</groupId>
            <artifactId>ehcache-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- END Test dependencies -->
    </dependencies>

    <build>
        <finalName>nrg-anonymize-${project.version}</finalName>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>org.nrg.xnat.maven.libraries.release</id>
            <name>XNAT Maven Release Libraries</name>
            <url>${maven.server.root}/libs-release</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>org.nrg.xnat.maven.libraries.snapshot</id>
            <name>XNAT Maven Snapshot Libraries</name>
            <url>${maven.server.root}/libs-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

 </project>
