/*
 * org.nrg.dcm.test.ScriptTableDAOTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm.test;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.apache.commons.io.FileUtils;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.iod.module.macro.Code;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.dcm.Anonymize;
import org.nrg.dcm.ScriptTableException;
import org.nrg.dcm.xnat.ScriptTable;
import org.nrg.dcm.xnat.ScriptTableDAO;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.junit.runner.RunWith;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import com.google.common.io.Files;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/org/nrg/dcm/test/TestContext.xml")
public class ScriptTableDAOTest {
	final static File testDataDir = new File("src/test/data");
	final static File dcmFile = new File(testDataDir, "test.dcm");
	final static File sampleDcmFile = new File(testDataDir, "sample.dcm");	
	final static File anonScript = new File(testDataDir, "dicom.das");
	
	static {
		assert(testDataDir.isDirectory());
		assert(sampleDcmFile.isFile());
		assert(anonScript.isFile());
	}
	
	@Before
	public void setUp() throws Exception {
		if (!dcmFile.isFile()) {
			Files.copy(sampleDcmFile, dcmFile);
		}
	}
	
	@After
	public void tearDown() throws Exception {
		if (dcmFile.isFile()) {
			FileUtils.deleteQuietly(dcmFile);
			// dcmFile.delete();
		}
	}
	
	private void createGoodDb() {
		_scriptTableDAO.create(this.createScriptTable(new Long(1), "", "proj1owner1"));
		_scriptTableDAO.create(this.createScriptTable(new Long(1), "v1", "proj1owner2"));
		_scriptTableDAO.create(this.createScriptTable(new Long(1), "v2", "proj2owner1"));
		_scriptTableDAO.create(this.createScriptTable(null, "", "admin"));
		_scriptTableDAO.create(this.createScriptTable(null, "v1", "admin2"));
	}
	
	private void createDb() throws IOException {
		String anonString = FileUtils.readFileToString(anonScript);
		_scriptTableDAO.create(this.createScriptTable(null, anonString, "admin"));
	}
	
	@Test
	@Transactional
	public final void testRunDbAnonymize() {
		try {
			createDb();
			_scriptTableDAO.anonymize(dcmFile, null, null, "subj1", "sess1");
			ScriptTable st = _scriptTableDAO.get(null);
			// test that the edit script applied by checking the history
			DicomInputStream dis = new DicomInputStream(new FileInputStream(dcmFile));
			DicomObject o = dis.readDicomObject();
			DicomElement e = o.get(Anonymize.RecordTag);
			Code[] codes = Code.toCodes(e);
			assertEquals(codes.length, 1);
			DicomObject c = codes[0].getDicomObject();
			assertEquals(c.getString(Tag.CodeMeaning), Anonymize.Meaning);
			assertEquals(c.getString(Tag.CodingSchemeDesignator), Anonymize.Designator);
			assertEquals(c.getString(Tag.CodingSchemeVersion), Anonymize.Version);
			assertEquals(st.getScript(), FileUtils.readFileToString(anonScript));
			dis.close();
		}
		catch (IOException e) {
			fail(e.getMessage());
		}
	}
	
	@Transactional
	private ScriptTable createScriptTable(Long project_id, String script, String user) {
		ScriptTable st = new ScriptTable();
		st.setProjectId(project_id);
		st.setScript(script);
		st.setXnatUser(user);
		return st;
	}
	
	private void createBadDb () {
		_scriptTableDAO.create(this.createScriptTable(new Long(1), "", "proj1owner1"));
		_scriptTableDAO.create(this.createScriptTable(new Long(1), null, "proj1owner2"));
		_scriptTableDAO.create(this.createScriptTable(new Long(2), null, "proj2owner1"));
		_scriptTableDAO.create(this.createScriptTable(new Long(1), "v2", "proj1owner1"));
		//no site-wide anon script
	}
	
	@Test
	@Transactional
	public final void testGetAllScriptTable() {
		createGoodDb();
		List<ScriptTable> all = _scriptTableDAO.getAll();
		assertEquals(all.size(), 5);
	}
	
	@Test
	@Transactional
	public final void testSanityCheckOnBadDb () {
		createBadDb();
		List<ScriptTableException> problems = _scriptTableDAO.findProblems();
		assertEquals(problems.size(),2);
		List<ScriptTable> nullScripts = problems.get(1).problem_scripts;
		assertEquals(nullScripts.size(),2);
		assertEquals(new Long(1), nullScripts.get(0).getProjectId());
		assertEquals(new Long(2), nullScripts.get(1).getProjectId());
	}
	
	@Test
	@Transactional
	public final void testSanityCheckOnGoodDb () {
		createGoodDb();
		List<ScriptTableException> problems = _scriptTableDAO.findProblems();
		assertEquals(problems.size(),0);
	}
	
	@Test
	@Transactional
	public final void testGetScriptTableByProject(){
		createGoodDb();
		assertEquals(_scriptTableDAO.getByProject(new Long(1)).size(),3);
		assertEquals(_scriptTableDAO.getByProject(null).size(),2);
	}
	

	@Test
	@Transactional
	public final void testGetLatestScript() {
		createGoodDb();
		assertEquals("v2", _scriptTableDAO.get(new Long(1)).getScript());
		assertEquals("v1", _scriptTableDAO.get(null).getScript());
	}
	
	@Test
	@Transactional
	public final void testRunDbAnonymization() {
		createGoodDb();
		try {
			
		}
		catch (Exception e) {
			
		}
 	}

	@Test
	@Transactional
	public final void testInsertScript() {
		createGoodDb();
		try {
			_scriptTableDAO.insertScript(new Long(1), null, "proj1owner1");
			fail();
		}
		catch (ScriptTableException e) {
			
		}
		catch (Exception e) {
			fail("Threw a general exception " + e.getMessage());
		}
		try {
			_scriptTableDAO.insertScript(new Long(1), "v4", "proj1owner1");
			assertEquals("v4",_scriptTableDAO.get(new Long(1)).getScript());
		}
		catch (ScriptTableException e) {
			fail("Threw ScriptTableException");
		}
	}
	
	@Test
	public final void dataSourceTest() throws SQLException {
        assertNotNull(_dataSource);
        Connection connection = _dataSource.getConnection();
        Statement statement = connection.createStatement();
        statement.execute("DROP TABLE IF EXISTS TEST");
        statement.execute("CREATE TABLE TEST(ID INT PRIMARY KEY, NAME VARCHAR(255))");
        statement.execute("INSERT INTO TEST VALUES(1, 'Hello')");
        statement.execute("INSERT INTO TEST VALUES(2, 'World')");
        statement.execute("SELECT * FROM TEST ORDER BY ID");
        ResultSet results = statement.getResultSet();
        int index = 1;
        while(results.next()) {
            int id = results.getInt("ID");
            String name = results.getString("NAME");
            assertEquals(index, id);
            assertEquals(index == 1 ? "Hello" : "World", name);
            index++;
        }
        statement.execute("DROP TABLE TEST");
	}
	@Autowired
	private ScriptTableDAO _scriptTableDAO;
	 
	@Inject
	@Named("dataSource")
	private DataSource _dataSource;
	
}
