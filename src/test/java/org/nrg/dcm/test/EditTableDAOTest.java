/*
 * org.nrg.dcm.test.EditTableDAOTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.dcm.EditTableException;
import org.nrg.dcm.xnat.EditTable;
import org.nrg.dcm.xnat.EditTableDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/org/nrg/dcm/test/TestContext.xml")
public class EditTableDAOTest {
	public void createGoodDb() {
		_editTableDAO.create(createEditTable(null, true, "admin"));
		_editTableDAO.create(createEditTable(1L, true, "proj1owner1"));
		_editTableDAO.create(createEditTable(2L, true, "proj2owner1"));
		_editTableDAO.create(createEditTable(null, false, "admin1"));
	}
	
	public void createBadDb() {
		// no null project
		// proj1 is set to true twice without it being set to false in-between
		// proj3's first row is set to false.
		_editTableDAO.create(createEditTable(1L, true, "proj1owner1"));
		_editTableDAO.create(createEditTable(1L, true, "proj1owner2"));
		_editTableDAO.create(createEditTable(2L, true, "proj2owner1"));
		_editTableDAO.create(createEditTable(3L, false, "proj3owner1"));
	}
	
	@Test
	@Transactional
	public void testGetProjectMap() {
		createGoodDb();
		Map<Long, List<EditTable>> projectMap = _editTableDAO.getProjectMap();
		assertEquals(projectMap.keySet().size(), 3);
		assertEquals(projectMap.get(null).size(), 2);
		assertEquals(projectMap.get(1L).size(), 1);
		assertEquals(projectMap.get(2L).size(), 1);
		assertEquals(projectMap.get(null).get(0).getXnatUser(), "admin");
		assertEquals(projectMap.get(null).get(1).getXnatUser(), "admin1");
	}
	
	@Test
	@Transactional
	public void testGetProject() {
		createBadDb();
		EditTable e = _editTableDAO.get(1L);
		assertEquals(e.getXnatUser(), "proj1owner2");
	}
	
	@Test
	@Transactional
	public void testEnable() {
		createGoodDb();
		EditTable a = _editTableDAO.get(null);
		assertEquals(a.getEdit(), false);
		EditTable e = new EditTable();
		e.setEdit(true);
		e.setProjectId(null);
		e.setXnatUser("admin");
		_editTableDAO.create(e);
		List<EditTable> rows = _editTableDAO.getByProject(null);
		assertEquals(rows.size(),3);
		EditTable f = _editTableDAO.get(null);
		assertEquals(f.getEdit(), true);
	}
	
	@Test
	@Transactional
	public void testDisable() {
		createGoodDb();
		EditTable a = _editTableDAO.get(1L);
		assertEquals(a.getEdit(), true);
		EditTable e = new EditTable();
		e.setEdit(true);
		e.setProjectId(1L);
		e.setXnatUser("proj1owner2");
		_editTableDAO.create(e);
		List<EditTable> rows = _editTableDAO.getByProject(1L);
		assertEquals(rows.size(),2);
		EditTable f = _editTableDAO.get(null);
		assertEquals(f.getEdit(), false);
	}
	
	@Test
	@Transactional
	public void testSanityCheckOnBadDb() {
		createBadDb();
		List<EditTableException> problems = _editTableDAO.findProblems();
		assertEquals(problems.size(), 3);
	}
	
	@Transactional
	private EditTable createEditTable(Long project_id, boolean edit, String user) {
		EditTable et = new EditTable();
		et.setProjectId(project_id);
		et.setEdit(edit);
		et.setXnatUser(user);
		return et;
	}

	@Autowired
	private EditTableDAO _editTableDAO;
}
