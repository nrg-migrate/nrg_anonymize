/*
 * org.nrg.dcm.EditTableException
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm;

import java.util.ArrayList;
import java.util.List;

import org.nrg.dcm.xnat.EditTable;
import org.nrg.dcm.xnat.ScriptTable;

public class EditTableException extends Throwable {
	
	public final List<EditTable> problem_scripts = new ArrayList<EditTable>();

	public EditTableException() {
	}
	
	public EditTableException(String message) {
		super(message);
	}

	public EditTableException(Throwable cause) {
		super(cause);
	}

	public EditTableException(String message, Throwable cause){
		super(message, cause);
	}

	public EditTableException(String message, List<EditTable> ss) {
		super(message);
		this.problem_scripts.addAll(ss);
	}

	public EditTableException(Throwable cause, List<EditTable> ss) {
		super(cause);
		this.problem_scripts.addAll(ss);
	}

	public EditTableException(String message, Throwable cause, List<EditTable> ss) {
		super(message, cause);
		this.problem_scripts.addAll(ss);
	}
}
