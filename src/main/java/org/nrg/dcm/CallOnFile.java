/*
 * org.nrg.dcm.CallOnFile
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm;

import java.io.File;
import java.util.concurrent.Callable;

import org.nrg.transaction.RollbackException;
import org.nrg.transaction.Transaction;
import org.nrg.transaction.TransactionException;

public abstract class CallOnFile<A> implements Callable<A> {
	File f = null;
	void setFile(File f) {
		if (!f.exists()) {
			File newFile = new File(f.getParent());
			newFile.mkdirs();
		}
		f.setWritable(true);
		new File(f.getParent()).setReadable(true);
		new File(f.getParent()).setWritable(true);
		this.f = f;
	}
	File getFile() {
		return this.f;
	}
}
