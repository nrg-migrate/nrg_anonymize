/*
 * org.nrg.dcm.WorkOnCopyOp
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 12/11/13 3:34 PM
 */
package org.nrg.dcm;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.nrg.transaction.RollbackException;
import org.nrg.transaction.Transaction;
import org.nrg.transaction.TransactionException;

public final class WorkOnCopyOp extends Transaction {
	
	final File src;
	final File tmpdir;
	CallOnFile<java.lang.Void> cof;
	LoggerI logger;

	WorkOnCopyOp (File src, File tmpdir, CallOnFile<java.lang.Void> cof) {
		this.src = src;
		this.tmpdir=tmpdir;
		this.cof = cof;
		this.logger = new LoggerI() {
			public void info (String l) {}
			public void failed (String l) {}
		};
	}
	WorkOnCopyOp (File src, File tmpdir, CallOnFile<java.lang.Void> cof, LoggerI l) {
		this.src = src;
		this.tmpdir=tmpdir;
		this.cof = cof;
		this.logger = l;
	}
	
	@Override
	public void run() throws TransactionException {
		try {
			final long ms=Calendar.getInstance().getTimeInMillis();
			this.cof.setFile(new File(tmpdir.getAbsolutePath(),ms+src.getName()));
			this.cof.call();

			String originalPath = src.getAbsolutePath();
			if (!src.delete()) {
				throw new RollbackException("Unable to delete " + originalPath + ". A backup exists at " + this.cof.getFile().getAbsolutePath());
			}
			// Simple renameTo fails on Windows if the destination directory exists
			// So we have to copy the original directory to the destination and then 
			// delete the original. There has to be a better way to do this.
			try {
				FileUtils.copyFile(this.cof.getFile(), new File(originalPath));
			}
			catch (IOException e) {
				throw new RollbackException(e);
			}
			if (!this.cof.getFile().delete()) {
                throw new RollbackException("Unable to delete " + this.cof.getFile().getAbsolutePath());
			}
		}
		catch (Throwable e) {
			throw new TransactionException(e);
		}
	}

	@Override
	public void rollback() throws RollbackException {
		File tmp = new File (tmpdir.getAbsolutePath(),src.getName());
		if (tmp.exists()) {
			if (tmp.isDirectory()) {
				try {
					FileUtils.deleteDirectory(tmp);
				}
				catch (IOException e) {
					throw new RollbackException(e);
				}
			}
			else {
                try {
                    FileUtils.forceDelete(tmp);
                } catch (IOException e) {
                    throw new RollbackException(e);
                }
            }
		}
	}
}
