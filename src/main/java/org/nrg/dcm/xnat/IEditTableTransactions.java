/*
 * org.nrg.dcm.xnat.IEditTableTransactions
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm.xnat;

import java.util.List;

public interface IEditTableTransactions {
	public List<EditTable> getByProject(String project);
	public List<EditTable> getAll();
	public EditTable getMostRecentRow(String project);
	public EditTable get(String project);
}
