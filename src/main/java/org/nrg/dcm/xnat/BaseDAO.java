/*
 * org.nrg.dcm.xnat.BaseDAO
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/5/13 1:10 PM
 */
package org.nrg.dcm.xnat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.framework.orm.hibernate.BaseHibernateEntity;
import org.springframework.transaction.annotation.Transactional;

public class BaseDAO<T extends BaseHibernateEntity> extends AbstractHibernateDAO<T> {

    protected BaseDAO() {
        super();
    }

    /**
     * Get the most recent row associated with the given project
     *
     * @param project The project to query.
     * @return The most recent row for the indicated project.
     */
    @Transactional
    public T getMostRecentRow(final Long project) {
        return this.getMostRecentRow(this.getByProject(project));
    }

    /**
     * Get the oldest row from the given list.
     *
     * @param rows The list of rows to search.
     * @return The oldest row from the given list.
     */
    @Transactional
    public T getOldestRow(final List<T> rows) {
        final List<T> sorted = sortRows(rows);
        return sorted.size() == 0 ? null : sorted.get(0);
    }

    /**
     * Get the oldest row from the given list.
     *
     * @param rows The list of rows to search.
     * @return The oldest row from the given list.
     */
    @Transactional
    public T getMostRecentRow(final List<T> rows) {
        final List<T> sorted = sortRows(rows);
        return sorted.size() == 0 ? null : sorted.get(sorted.size() - 1);
    }

    /**
     * Sort the given list of rows into ascending order by the row id.
     *
     * @param rows The list of rows to search.
     * @return The list of rows sorted by ID.
     */
    public List<T> sortRows(final List<T> rows) {
        final List<T> sorted = new ArrayList<>();
        sorted.addAll(rows);
        Collections.sort(sorted, new Comparator<T>() {
            public int compare(T t1, T t2) {
                return ((Long) t1.getId()).compareTo(t2.getId());
            }
        });
        return sorted;
    }

    /**
     * Get all the entities where the given column is null.
     *
     * @param column    The column to check for null.
     * @return A list of all entities where the value for column is set to null.
     */
    @SuppressWarnings("unchecked")
    public List<T> getAllNull(final String column) {
        Criteria criteria = getCriteriaForType();
        criteria.add(Restrictions.eq("enabled", true));
        criteria.add(Restrictions.isNull(column));

        List list = criteria.list();

        if (list == null) {
            return new ArrayList<>();
        } else {
            final List<T> ret = new ArrayList<>();
            ret.addAll(list);
            return ret;
        }
    }

    /**
     * Rows cannot be deleted from the Script table or the Edit table. This method will thrown an
     * {@link UnsupportedOperationException} if called.
     */
    public void delete(final T t) {
        throw new UnsupportedOperationException("Cannot delete from the Script or the Edit table.");
    }

    /**
     * Retrieve all the rows with with the given project.
     *
     * @param projectId The ID of the project to search.
     * @return All objects of the parameterized type that are associated with the indicated project.
     */
    @SuppressWarnings("unchecked")
    @Transactional
    public List<T> getByProject(final Long projectId) {
        Criteria criteria = getCriteriaForType();
        criteria.add(Restrictions.eq("enabled", true));
        if (null == projectId) {
            criteria.add(Restrictions.isNull("projectId"));
        } else {
            criteria.add(Restrictions.eq("projectId", projectId));
        }

        List list = criteria.list();

        if (list == null) return new ArrayList<>();
        else {
            List<T> ret = new ArrayList<>();
            ret.addAll(list);
            return ret;
        }
    }

    /**
     * Retrieve the entire table.
     *
     * @return All of the objects of the parameterized type.
     */
    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        Criteria criteria = getCriteriaForType();
        @SuppressWarnings("rawtypes")
        List list = criteria.list();
        List<T> ret = new ArrayList<>();
        ret.addAll(list);
        return ret;
    }
}
