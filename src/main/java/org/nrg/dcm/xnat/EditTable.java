/*
 * org.nrg.dcm.xnat.EditTable
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/4/13 4:37 PM
 */
package org.nrg.dcm.xnat;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.annotations.Auditable;

import javax.persistence.Entity;

@Auditable
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
public class EditTable extends AbstractHibernateEntity{
    private static final long serialVersionUID = 483068123474236528L;
    public Long project_id;
	public boolean edit;
	public String xnatUser;

	public Long getProjectId() {
		return project_id;
	}
	public void setProjectId(Long project) {
		this.project_id = project;
	}
	public boolean getEdit() {
		return edit;
	}
	public void setEdit(boolean edit) {
		this.edit = edit;
	}
	public String getXnatUser() {
		return xnatUser;
	}
	public void setXnatUser(String user) {
		this.xnatUser = user;
	}
}
