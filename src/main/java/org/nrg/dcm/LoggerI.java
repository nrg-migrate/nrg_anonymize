/*
 * org.nrg.dcm.LoggerI
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm;

public interface LoggerI {
	void info(String l);
	void failed(String l);
}